
create-virtual-env:
	python -m venv .env

unload-env:
	deactivate

install-requirements:
	python setup.py install

test:
	python setup.py test

integration-test:
	export PYTHONPATH=$(PWD) && python iomete_jdbc_sync/test/integration_test.py
