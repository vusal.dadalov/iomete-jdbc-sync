from setuptools import setup, find_packages

setup(
    name='iomete_jdbc_sync',
    version='1.0.0',
    packages=find_packages(exclude=("*test*",)),

    setup_requires=['pytest-runner'],
    tests_require=['pytest']
)
