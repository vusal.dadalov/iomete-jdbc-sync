import structlog
from ..connection.destination import WarehouseConnection
from pyhive import hive
from sqlalchemy import create_engine, inspect


logger = structlog.get_logger()


class WarehouseDB:
    def __init__(self,
                 warehouse_connection: WarehouseConnection):
        self.warehouse_connection = warehouse_connection
        self.inspector = inspect(create_engine(warehouse_connection.sql_alchemy_url))
        self.__conn = self.__create_connection()

    def __get_cursor(self):
        return self.__conn.cursor()

    def __create_connection(self):
        logger.debug("Creating default database connection...")
        default_db_connection = hive.connect(host=self.warehouse_connection.host,
                                             port=self.warehouse_connection.port,
                                             database="default",
                                             username=self.warehouse_connection.user_name,
                                             password=self.warehouse_connection.password,
                                             auth='CUSTOM',
                                             transport_mode='https')

        logger.debug("create database if not exists", database_name=self.warehouse_connection.db_name)
        default_db_connection.cursor().execute(
            f"CREATE DATABASE IF NOT EXISTS {self.warehouse_connection.db_name}"
        )

        logger.info("Creating target database connection", database_name=self.warehouse_connection.db_name)
        return hive.connect(host=self.warehouse_connection.host,
                            port=self.warehouse_connection.port,
                            database=self.warehouse_connection.db_name,
                            username=self.warehouse_connection.user_name,
                            password=self.warehouse_connection.password,
                            auth='CUSTOM',
                            transport_mode='https')

    def table_exists(self, table_name: str):
        db_name = self.warehouse_connection.db_name
        return db_name in self.inspector.get_schema_names() and \
               table_name.lower() in self.inspector.get_table_names(db_name)

    def table_column_names(self, table_name):
        db_name = self.warehouse_connection.db_name
        return [c['name'] for c in self.inspector.get_columns(table_name, db_name)]

    def execute(self, query):
        cursor = self.__get_cursor()
        logger.debug("Executing query", query=query)
        cursor.execute(query)

    def create_database_if_not_exists(self):
        self.execute(
            f"CREATE DATABASE IF NOT EXISTS {self.warehouse_connection.db_name}"
        )

    def query_single_value(self, query):
        cursor = self.__get_cursor()
        cursor.execute(query)
        result = cursor.fetchone()
        cursor.close()
        if result:
            return result[0]
        return None
