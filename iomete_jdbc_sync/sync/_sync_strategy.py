import structlog

from .sync_mode import SyncMode, FullLoad, IncrementalSnapshot
from ._warehouse_db import WarehouseDB

logger = structlog.get_logger()


class DataSyncFactory:
    @staticmethod
    def instance_for(sync_mode: SyncMode, warehouse_db: WarehouseDB):
        if isinstance(sync_mode, FullLoad):
            return FullLoadDataSync(warehouse_db)

        if isinstance(sync_mode, IncrementalSnapshot):
            return IncrementalSnapshotDataSync(warehouse_db, sync_mode)

        raise Exception(f"Unsupported load type: {sync_mode}")


class DataSync:
    def sync(self, proxy_view_name, staging_table_name):
        pass


class IncrementalSnapshotDataSync(DataSync):
    def __init__(self,
                 warehouse_db: WarehouseDB,
                 incremental_load_settings: IncrementalSnapshot):
        self.warehouse_db = warehouse_db
        self.incremental_load_settings = incremental_load_settings
        self.full_load_table_recreate_data_migration = FullLoadTableRecreateDataSync(warehouse_db)

    def sync(self, proxy_view_name, staging_table_name):
        logger.info("IncrementalSnapshotDataSync.sync", proxy_view_name=proxy_view_name,
                    staging_table_name=staging_table_name)
        if not self.warehouse_db.table_exists(staging_table_name):
            logger.info("Table doesn't exists. Doing full dump")
            self.full_load_table_recreate_data_migration.sync(proxy_view_name, staging_table_name)
            return

        id_column_name = self.incremental_load_settings.identification_column
        tracking_column = self.incremental_load_settings.tracking_column

        max_tracking_value = self.warehouse_db.query_single_value(
            query=f"select max({tracking_column}) from {staging_table_name}")

        proxy_view_column_names = self.warehouse_db.table_column_names(proxy_view_name)

        insert_columns = ",".join(proxy_view_column_names)
        insert_values = ",".join(f"src.{c}" for c in proxy_view_column_names)

        update_columns = ",".join([f"trg.{c}=src.{c}"
                                   for c in proxy_view_column_names if c != id_column_name])
        merge_query = f"""
                MERGE INTO {staging_table_name} trg
                USING (SELECT * FROM {proxy_view_name} 
                        WHERE {tracking_column} > '{max_tracking_value}') src
                ON (src.{id_column_name} = trg.{id_column_name})
                WHEN MATCHED THEN 
                    UPDATE SET {update_columns}
                WHEN NOT MATCHED THEN 
                    INSERT ({insert_columns}) VALUES({insert_values})
            """

        self.warehouse_db.execute(merge_query)


class FullLoadDataSync(DataSync):
    def __init__(self, warehouse_db: WarehouseDB):
        self.warehouse_db = warehouse_db
        self.full_load_table_recreate_data_migration = FullLoadTableRecreateDataSync(warehouse_db)

    def sync(self, proxy_view_name, staging_table_name):
        logger.info("FullLoadDataSync.sync", proxy_view_name=proxy_view_name, staging_table_name=staging_table_name)
        if not self.warehouse_db.table_exists(staging_table_name):
            logger.info("Table doesn't exists. Doing full dump")
            self.full_load_table_recreate_data_migration.sync(proxy_view_name, staging_table_name)
            return

        self.warehouse_db.execute(
            f"INSERT OVERWRITE TABLE {staging_table_name} SELECT * FROM {proxy_view_name}")


class FullLoadTableRecreateDataSync(DataSync):
    def __init__(self, warehouse_db: WarehouseDB):
        self.warehouse_db = warehouse_db

    def sync(self, proxy_view_name, staging_table_name):
        logger.info("FullLoadTableRecreateDataSync.sync", proxy_view_name=proxy_view_name,
                    staging_table_name=staging_table_name)
        self.warehouse_db.create_database_if_not_exists()

        self.warehouse_db.execute(
            f"CREATE TABLE {staging_table_name} USING delta AS SELECT * FROM {proxy_view_name}")
