from typing import List

import structlog
from ._warehouse_db import WarehouseDB

from ..connection.destination import WarehouseConnection
from ..connection.source import SourceConnection
from ..log_utils import configure_project_logger

from .sync_mode import SyncMode
from ._sync_strategy import DataSyncFactory

configure_project_logger(project_logger_name='src')

logger = structlog.get_logger()


class Table:
    def __init__(self, table_name: str, sync_mode: SyncMode):
        self.table_name = table_name
        self.sync_mode = sync_mode

    def __str__(self):
        return f"Table(table_name='{self.table_name}', sync_mode={self.sync_mode})"



class DataSyncer:
    def __init__(self, source_connection: SourceConnection,
                 warehouse_connection: WarehouseConnection,
                 tables: List[Table]):
        self.source_connection = source_connection
        self.warehouse_connection = warehouse_connection
        self.warehouse_db = WarehouseDB(self.warehouse_connection)
        self.tables = tables

    def run(self):
        logger.info("Data sync started...",
                    source_connection=str(self.source_connection))

        for table in self.tables:
            self.__migrate_table(table)

    @staticmethod
    def __proxy_table(table: Table):
        return f"__{table.table_name}_proxy"

    @staticmethod
    def __staging_table_name(table: Table):
        return table.table_name

    def __migrate_table(self, table: Table):
        logger.info("Syncing table", table=str(table))
        proxy_view_name = self.__proxy_table(table)
        staging_table_name = self.__staging_table_name(table)

        self.__create_proxy_table(table, proxy_view_name)

        data_sync = DataSyncFactory.instance_for(
            sync_mode=table.sync_mode, warehouse_db=self.warehouse_db
        )

        data_sync.sync(proxy_view_name, staging_table_name)

    def __create_proxy_table(self, table: Table, proxy_view_name):
        self.warehouse_db.create_database_if_not_exists()

        self.warehouse_db.execute(
            self.source_connection.proxy_table_definition(
                table_name=table.table_name,
                proxy_view_name=proxy_view_name))
