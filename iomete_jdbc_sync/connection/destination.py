class WarehouseConnection:
    def __init__(self, host: str, port: str, user_name: str, password: str, db_name: str):
        self.db_name = db_name
        self.host = host
        self.port = port
        self.user_name = user_name
        self.password = password

    @property
    def sql_alchemy_url(self):
        return f"hive://{self.user_name}:{self.password}@{self.host}:{self.port}/{self.db_name}?auth=CUSTOM;transport_mode=https"
