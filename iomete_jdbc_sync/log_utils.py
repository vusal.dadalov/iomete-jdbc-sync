import logging
import sys

import structlog
from pythonjsonlogger import jsonlogger
from structlog.dev import ConsoleRenderer

def configure_project_logger(project_logger_name: str):
    def prod_log_configuration():
        handler = logging.StreamHandler(sys.stdout)
        handler.setFormatter(jsonlogger.JsonFormatter())
        root_logger = logging.getLogger()
        root_logger.addHandler(handler)
        root_logger.setLevel(logging.INFO)

        project_logger = logging.getLogger(project_logger_name)
        project_logger.setLevel(logging.DEBUG)  # This toggles all the logging in your app

        structlog.configure(
            processors=[
                structlog.stdlib.filter_by_level,
                structlog.contextvars.merge_contextvars,
                structlog.stdlib.add_logger_name,
                structlog.stdlib.add_log_level,
                structlog.stdlib.PositionalArgumentsFormatter(),
                structlog.processors.StackInfoRenderer(),
                structlog.processors.format_exc_info,
                structlog.processors.UnicodeDecoder(),
                structlog.stdlib.render_to_log_kwargs,
            ],
            context_class=dict,
            logger_factory=structlog.stdlib.LoggerFactory(),
            wrapper_class=structlog.stdlib.BoundLogger,
            cache_logger_on_first_use=True,
        )

    def debug_log_configuration():
        handler = logging.StreamHandler()
        handler.setFormatter(structlog.stdlib.ProcessorFormatter(
            processor=structlog.dev.ConsoleRenderer(),
        ))

        root_logger = logging.getLogger()
        root_logger.addHandler(handler)
        root_logger.setLevel(logging.INFO)

        project_logger = logging.getLogger(project_logger_name)
        project_logger.setLevel(logging.DEBUG)  # This toggles all the logging in your app

        # The processor list
        processors = [
            structlog.stdlib.filter_by_level,
            structlog.stdlib.add_logger_name,
            structlog.stdlib.add_log_level,
            structlog.contextvars.merge_contextvars,

            structlog.stdlib.PositionalArgumentsFormatter(),
            structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M:%S.%f"),
            structlog.processors.StackInfoRenderer(),
            structlog.dev.set_exc_info,
            structlog.processors.format_exc_info,
            ConsoleRenderer(colors=True),
        ]

        structlog.configure_once(
            processors=processors,
            context_class=dict,  # or OrderedDict if the runtime's dict is unordered (e.g. Python <3.6)
            logger_factory=structlog.stdlib.LoggerFactory(),
            wrapper_class=structlog.stdlib.BoundLogger,
            cache_logger_on_first_use=True,
        )

    env = "development"
    if env == "development":
        debug_log_configuration()
    else:
        prod_log_configuration()
