import os

from iomete_jdbc_sync.connection.source import MySQLConnection

from iomete_jdbc_sync import DataSyncer

from iomete_jdbc_sync.connection.destination import WarehouseConnection
from iomete_jdbc_sync.sync.sync_mode import FullLoad, IncrementalSnapshot
from iomete_jdbc_sync import Table

if __name__ == '__main__':
    table_tbls = Table(
        table_name='TBLS',
        sync_mode=IncrementalSnapshot(
            identification_column="tbl_id",
            tracking_column="write_id")
    )

    # table_tbls = Table(
    #     table_name='TBLS',
    #     sync_mode=FullLoad()
    # )

    data_syncer = DataSyncer(
        source_connection=MySQLConnection(
            host="test-db-1",
            port="3306",
            schema="corssaccountdb",
            user_name="admin",
            user_pass=os.getenv("mysql_password")),
        warehouse_connection=WarehouseConnection(
            host="reporting-645049172474-dwh.iomete.com",
            port="443",
            user_name="vusal",
            password=os.getenv("warehouse_password"),
            db_name="corssaccountdb"
        ),
        tables=[table_tbls],
    )

    data_syncer.run()
