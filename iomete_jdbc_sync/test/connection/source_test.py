from iomete_jdbc_sync.connection.source import PostgreSQLConnection

from iomete_jdbc_sync.connection.source import MySQLConnection


def test_mysql_connection():
    mysql_conn = MySQLConnection("localhost", "3306", "mydb", "user1", "pass1")
    assert mysql_conn.proxy_table_definition("tbl1", "__tbl1_proxy").strip() == """
        CREATE TABLE IF NOT EXISTS __tbl1_proxy
            USING org.apache.spark.sql.jdbc
            OPTIONS (
              url 'jdbc:mysql://localhost:3306/mydb?zeroDateTimeBehavior=convertToNull',
              dbtable 'mydb.tbl1',
              user 'user1',
              password 'pass1',
              driver 'com.mysql.jdbc.Driver'
            )
    """.strip()


def test_postgresql_connection():
    mysql_conn = PostgreSQLConnection("localhost", "3306", "mydb", "user1", "pass1")
    assert mysql_conn.proxy_table_definition("tbl1", "__tbl1_proxy").strip() == """
        CREATE TABLE IF NOT EXISTS __tbl1_proxy
            USING org.apache.spark.sql.jdbc
            OPTIONS (
              url 'jdbc:postgresql://localhost:3306/mydb',
              dbtable 'mydb.tbl1',
              user 'user1',
              password 'pass1',
              driver 'org.postgresql.Driver'
            )
    """.strip()
