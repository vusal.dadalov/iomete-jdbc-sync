from iomete_jdbc_sync.connection.destination import WarehouseConnection


def test_warehouse_connection():
    warehouse_conn = WarehouseConnection("localhost", "10000", "user1", "pass1", "db1")
    assert warehouse_conn.sql_alchemy_url == "hive://user1:pass1@localhost:10000/db1?auth=CUSTOM;transport_mode=https"
